<?php
header("Content-Type: application/json");
require "config.php";

function error_info($code, $message, $type) {
    $response["status"] = false;
    $response["error"]["code"] = $code;
    $response["error"]["message"] = $message;
    $response["error"]["type"] = $type;
    echo json_encode($response);
    exit;
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (empty($_POST['group']) || !isset($arrGroup[$_POST['group']])) {
        error_info(101, "Choose group from list", "group");
    }
    if (empty($_POST['firstname'])) {
        error_info(102, "Input first name", "first-name");
    }
    if (empty($_POST['lastname'])) {
        error_info(103, "Input last name", "last-name");
    }
    if (empty($_POST['gender']) || !isset($arrGender[$_POST['gender']])) {
        error_info(104, "Choose gender from list", "gender");
    }
    if (empty($_POST['birthday'])) {
        error_info(105, "Choose date of birth calendar", "birth");
    }

    $response["status"] = true;
    $response["user"] = $_POST;
    echo json_encode($response);
    exit;
}
echo "Requested resource is forbidden";
?>