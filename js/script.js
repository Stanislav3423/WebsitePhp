window.addEventListener('load', async ()=>{
    if (navigator.serviceWorker){
        try {
            await navigator.serviceWorker.register('sw.js');
            console.log("Correct registration");
        }catch (e) {
            console.log("Service work fail");
        }
    }
});

const table = document.querySelector('.students-table');
let studentId = 1;

let Student = function() {
    this.id = "";
    this.group = "";
    this.firstname = "";
    this.lastname = "";
    this.gender = "";
    this.birthday = "";
    this.status = false;
}

document.addEventListener("DOMContentLoaded", function() {

    document.getElementById("student-form").addEventListener("submit", function(event) {
        event.preventDefault();
        let student = new Student();
        student.id = document.getElementById("id").value;
        student.group = document.getElementById("group").value;
        student.firstname = document.getElementById("first-name").value;
        student.lastname = document.getElementById("last-name").value;
        student.gender = document.getElementById("gender").value;
        student.birthday = document.getElementById("birth").value;
        student.status = Boolean(document.getElementById("status").checked);

        let json = JSON.stringify(student);
        console.log(json);

        addEditStudent(student);
    });

    document.getElementById("delete-submit").addEventListener('click', function(event) {
        let id = document.getElementById("deleteId").value;
        let row = getRowByDataAttribute('data-id',id);
        row.parentNode.removeChild(row);
        let model = bootstrap.Modal.getInstance(document.getElementById("delete-modal"));
        model.hide();
    });

    document.querySelector(".functional-part").addEventListener("click", function(event) {
        let button = event.target.closest("button");
        if (button?.classList.contains("add-edit-button")) {
            openEditAddModal(button);
        }
        else if (button?.classList.contains("delete-students-row-button")) {
            openDeleteModal(button);
        }
    });
});

function openEditAddModal(button) {
    clearValidation();
    let student = new Student();
    let title = "Add student";
    let button_text = "Add";

    if (button.getAttribute("data-id") !== "") {
        title = "Edit student";
        button_text = "Edit"
        let tr = button.closest('tr');
        let columns = tr.querySelectorAll('td');
        let isActive;
        columns.forEach(column => {
            if(column.querySelector('i.status')) {
                isActive = column.querySelector('i.status').classList.contains('active');
            }
        });

        student.id = tr.getAttribute("data-id");
        student.group = columns[1].getAttribute("data-value");
        let name = columns[2].textContent.split(" ");
        student.firstname = name[0];
        student.lastname = name[1];
        student.gender = columns[3].getAttribute("data-value");
        student.birthday = columns[4].textContent;
        student.status = isActive;
    }

    document.getElementById("id").value = student.id ? student.id: "";
    document.getElementById("group").value = student.group;
    document.getElementById("first-name").value = student.firstname ? student.firstname: "";
    document.getElementById("last-name").value = student.lastname ? student.lastname: "";
    document.getElementById("gender").value = student.gender;
    document.getElementById("birth").value = student.birthday ? transformDateForModal(student.birthday) : "";
    document.getElementById("status").checked = student.status;

    document.getElementById("add-edit-submit").innerText = button_text;
    document.getElementById("ModalLabel").innerText = title;
    let modal = new bootstrap.Modal(document.getElementById('student-modal'))
    modal.show();
}

function clearValidation(){
    document.querySelectorAll('.is-invalid').forEach(element => {
        element.classList.remove('is-invalid');
    })
}

function addEditStudent(student) {
    console.log(student);
    $.ajax({
        url: 'validation.php',
        type: 'POST',
        data: student,
        dataType: 'json',
        success: function(data) {
            console.log(data);
            clearValidation();
            if (data.status){
                if (student.id === "") {
                    student.id = studentId;
                    studentId++;
                    addStudent(student);
                } else {
                    editStudent(student);
                }
                let model = bootstrap.Modal.getInstance(document.getElementById("student-modal"));
                model.hide();
            }
            else {
                document.getElementById(data.error.type).classList.add("is-invalid");
            }
        },
        error: function(status, error) {
            console.error(status+': '+error)
        }
    })
}

function addStudent(student) {
    let newRow = document.createElement('tr');
    newRow.setAttribute("data-id", student.id);
    let status;
    if (student.status) {
        status = '<i class="bi bi-circle-fill status active"></i>';
    } else {
        status = '<i class="bi bi-circle-fill status"></i>';
    }
    newRow.innerHTML = 
        `<td><input type="checkbox" class="table-input"></td>
        <td data-value = "${student.group}">${document.querySelector('#group option[value="'+student.group + '"]').textContent}</td>
        <td>${student.firstname + " " +student.lastname}</td>
        <td data-value = "${student.gender}">${document.querySelector('#gender option[value="'+student.gender + '"]').textContent}</td>
        <td>${transformDateForTable(student.birthday)}</td>
        <td>${status}</td>
        <td>
            <div class="d-flex justify-content-center align-items-center mx-auto">
                <button class="btn-icon me-2 add-edit-button" data-id="${student.id}">
                    <i class="bi bi-pencil table-icons"></i>
                </button>
                <button class="btn-icon delete-students-row-button" data-id="${student.id}">
                    <i class="bi bi-x-lg table-icons"></i>
                </button>
            </div>
        </td>`;
    table.getElementsByTagName("tbody")[0].appendChild(newRow);
}

function getRowByDataAttribute(attname, attvalue){
    let rows = table.getElementsByTagName('tr');
    for (let i = 0; i<rows.length; i++){
        if (rows[i].getAttribute(attname) == attvalue) {
            return rows[i]
        }
    }
}

function editStudent(student) {
    let row = getRowByDataAttribute('data-id', student.id);
    let columns = row.querySelectorAll('td');

    columns[1].textContent = document.querySelector('#group option[value="'+student.group + '"]').textContent;
    columns[2].textContent = student.firstname + " " +student.lastname;
    columns[3].textContent = document.querySelector('#gender option[value="'+student.gender + '"]').textContent;
    columns[4].textContent = transformDateForTable(student.birthday);
    if (student.status) {
        columns[5].innerHTML = '<i class="bi bi-circle-fill status active"></i>';
    } else {
        columns[5].innerHTML = '<i class="bi bi-circle-fill status"></i>';
    }

    columns[1].setAttribute("data-value", student.group);
    columns[3].setAttribute("data-value", student.gender);
}

function transformDateForModal(dateString) {
    let parts = dateString.split('.');
    let transformedDate = parts[2] + '-' + parts[1] + '-' + parts[0];
    return transformedDate;
}

function transformDateForTable(dateString) {
    let parts = dateString.split('-');
    let transformedDate = parts[2] + '.' + parts[1] + '.' + parts[0];
    return transformedDate;
}

function openDeleteModal(button) {
    let row = button.closest('tr');
    let columns = row.querySelectorAll('td');
    let name = columns[2].textContent.trim();
    document.getElementById("deleteId").value = button.getAttribute("data-id");
    document.getElementById("delete-message").innerText = "Are you sure you want to delete student "+name+"?"; 
    let modal = new bootstrap.Modal(document.getElementById('delete-modal'))
    modal.show();
}